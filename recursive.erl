-module(recursive).
-compile(export_all).

len([]) -> 0;
len([_|T]) -> 1 + len(T).

% only interesting method...we don't need to export tail_fac/2
tail_fac(N) -> tail_fac(N,1).

tail_fac(0, Acc) -> Acc;
tail_fac(N, Acc) when N > 0 -> tail_fac(N-1, N*Acc).


len_tail([]) -> 0;
len_tail(N) -> len_tail(N, 0).

len_tail([], L) -> L;
len_tail([_|Tail], L) -> len_tail(Tail, L+1).

duplicate(0, _) -> [];
% Could get performance bottleneck
% duplicate(N, Term) -> [Term | duplicate(N-1, Term)].
% Tail recursion
duplicate(N, Term) -> duplicate(N, Term, []).

duplicate(0, _, Acc) -> Acc;
duplicate(N, Term, Acc) when N > 0 -> duplicate(N - 1, Term, [Term | Acc]).


reverse([]) -> [];
reverse([H|T]) -> reverse(T) ++ [H].

tail_reverse(L) -> tail_reverse(L, []).

tail_reverse([], Acc) -> Acc;
tail_reverse([H|T], Acc) -> tail_reverse(T, [H|Acc]).


% Given a List: L and intger: N
% Return first N values from L
sublist([], _) -> [];
sublist(_, 0) -> [];
sublist(L, N) -> sublist(L, N, []).

sublist([], _, Acc) -> Acc;
sublist(_, N, Acc) when N < 1 -> Acc;
sublist([H|T], N, Acc) when N > 0 -> 
  sublist(T, N-1, Acc ++ [H]).

% We want to take two list and create tuples for them
% recursive:zip([a,b,c], [1,2,3]) => [{a,1}, {b,2}, {c,3}]
zip(_, []) -> {};
zip([], _) -> {};
zip([K|Kt], [V|Vt]) -> zip(Kt, Vt, {K,V}).

zip(_, [], Tuples) -> Tuples;
zip([], _, Tuples) -> Tuples;
zip([Kh|Kt], [Vh|Vt], Tuples) -> zip(Kt, Vt, [{Kh, Vh}|Tuples]).


quicksort([]) -> [];
quicksort([Pivot|Rest]) ->
  {Smaller, Larger} = partition(Pivot, Rest, [], []),
  io:fwrite("Smaller: ~w~n", [ Smaller]),
  io:fwrite("Larger: ~w~n", [ Larger ]),
  quicksort(Smaller) ++ [Pivot] ++ quicksort(Larger).

partition(_, [], Smaller, Larger) -> {Smaller, Larger};
partition(Pivot, [H|T], Smaller, Larger) ->
  if H =< Pivot -> partition(Pivot, T, [H|Smaller], Larger);
    H > Pivot -> partition(Pivot, T, Smaller, [H|Larger])
  end.
