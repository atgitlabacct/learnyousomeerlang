-module(what_if).
-compile(export_all).

heh_fine() ->
  if
    1=:= 1 ->
      works
  end,
  if
    % essentially an OR clause
    1 =:= 2; 1 =:= 1 ->
      works
  end,
  if
    % essentially an AND  clause
    1 =:= 2, 1 =:= 1 ->
      fails
  end.


oh_god(N) ->
  if 
    N =:= 2 -> 
      might_succeed;
    N =:= 3 -> 
      might_succeed;
    true -> always_does % what is returned if nothing else succeeds.
  end.

help_me(Animal) ->
  Talk = if
    Animal == cat ->"meow";
    Animal == beef -> "moo";
    Animal == dog -> "bark";
    Animal == tree -> "bark";
    true -> "fgdadfgna"
  end,
  {Animal, "says " ++ Talk ++ "!"}.
  

insert(X, []) ->
  [X];
insert(X, Set) ->
  % lists:member/2 checks to see if X is in the Set
  case lists:member(X, Set) of
    true -> Set;
    false -> [X|Set]
  end.


beach(Temperature) ->
  case Temperature of
    {celsius, N} when N >= 20, N =< 45 ->
      'favorable';
    {kelvin, N} when N >= 293, N =< 318 ->
      'scientifically favorable';
    {fahrenheit, N} when N >= 68, N =< 113 ->
      'favorable in the US';
    _ ->
      'avoid the beach'
  end.

% Alternative to using if/case syntax
% Why not just pattern match!
beach_fun({celsius, N}) when N >= 20, N =< 45 ->
  'favorable';
beach_fun({kelvin, N}) when N >= 293, N =< 318 ->
  'favorable';
beach_fun({fahrenheit, N}) when N >= 68, N =< 113 ->
  'favorable in the US';
beach_fun(_) -> "don't go to the beach".
