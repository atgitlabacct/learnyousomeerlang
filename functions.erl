-module(functions).
-compile(export_all).

head([H|_]) -> H.
second([_,X|_]) -> X.

same(X,X) -> true;
same(_,_) -> false.

valid_time({Date = {Y, M, D}, Time = {H, Min, S}}) ->
  io:format("The time is ~p/~p/~p ~p:~p:~p~n", [Y, M, D, H, Min, S]);
valid_time(_) ->
  io:format("Invalid Date and Time~n").


old_enough(X) when X > 15 ->
  io:format("You can drive~n");
old_enough(X) when X < 16 ->
  io:format("You can't drive~n").
